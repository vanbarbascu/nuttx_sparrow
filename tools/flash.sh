
if [ -z "$1" ]
  then
    echo "Usage: flash.sh --bootloader # To flash the Arduino bootloader using AVR ISP."
    echo "       flash.sh --port <port> # To flash NuttX using serial port."
fi

case "$1" in
  --bootloader)
    avrdude -p m128rfa1 -P usb -c avrispmkII -U flash:w:tools/bootloader.hex:i -F -vvv
    ;;
  --port)
    avrdude -p m128rfa1 -P $2 -c arduino -U flash:w:nuttx.hex:i -F -vvv -b 57600
    ;;
  *)
    echo "Usage: flash.sh --bootloader # To flash the Arduino bootloader using AVR ISP."
    echo "       flash.sh --port <port> # To flash NuttX using serial port."
    ;;
esac
